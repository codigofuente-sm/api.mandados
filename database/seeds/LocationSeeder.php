<?php

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->delete();
        $now = date('Y-m-d H:i:s');

        $data = array(
            [
                'lat' => '11.2265519',
                'lng' => '-74.1966473,13.75',
                'created_at' => $now,
                'updated_at' => $now,
                'deleted_at' => NULL,
            ]
        );

        DB::table('locations')->insert($data);
    }
}
